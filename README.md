# Video Poker

## Game Flow:

### Pre-Game
* card buttons are disabled
* see payouts change as the player changes their bet

- player incements or decrements their bet
- player hits Deal button

### Game
* increase and decrease bet buttons are disabled, card buttons are enabled
* 5 cards are drawn from the deck and shown in the players hand

- player selects or unselects cards from their hand to mark with Hold
- player hits Draw button

### Post-Game
* all cards and buttons are disabled
* unselected cards are replaced with new cards from the deck

- wait one second

* player is shown what pattern they matched and which cards matched that pattern and how many credits they won (if any)
* update player credits
* new button is enabled

- player hits New button




straight including both jokers
1 j j 4 5 ~~ 5-1+1=5 == 5 = true

straight using one joker
6 j 8 9 j ~~ 9-6+1=4 == 5 = false

straight using neither joker
4 5 6 j j ~~ 6-4+1=3 == 5 = false
