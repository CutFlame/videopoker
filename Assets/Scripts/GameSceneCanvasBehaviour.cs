﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
//#nullable enable

public class GameSceneCanvasBehaviour : MonoBehaviour
{
    #region Outlets

    public TextMeshProUGUI patternListLabel;
    public TextMeshProUGUI payoutListLabel;

    public Button decreaseBetButton;
    public Button increaseBetButton;
    public TextMeshProUGUI betLabel;
    public TextMeshProUGUI creditsLabel;

    public Button playButton;
    public TextMeshProUGUI playButtonLabel;

    public Button cardSlot1Button;
    public Button cardSlot2Button;
    public Button cardSlot3Button;
    public Button cardSlot4Button;
    public Button cardSlot5Button;

    public TextMeshProUGUI cardSlot1Label;
    public TextMeshProUGUI cardSlot2Label;
    public TextMeshProUGUI cardSlot3Label;
    public TextMeshProUGUI cardSlot4Label;
    public TextMeshProUGUI cardSlot5Label;

    #endregion

    private GameModel gameModel;

    private Button[] cardSlotButtons;
    private TextMeshProUGUI[] cardSlotLabels;
    private Dictionary<byte, Sprite> spriteForCardIdLookup = new Dictionary<byte, Sprite>();
    private Sprite spriteForCardSlot;

    // Start is called just before any of the Update methods is called the first time
    private void Start() {
        //forward console logs to Unity Editor
        UnitySystemConsoleRedirector.Redirect();

        //run unit tests
        PatternMatchingMethodsTests.run();

        cardSlotButtons = new[] {
            cardSlot1Button,
            cardSlot2Button,
            cardSlot3Button,
            cardSlot4Button,
            cardSlot5Button,
        };
        cardSlotLabels = new TextMeshProUGUI[] {
            cardSlot1Label,
            cardSlot2Label,
            cardSlot3Label,
            cardSlot4Label,
            cardSlot5Label,
        };

        loadSprites();

        //Setup the GameModel
        var rules = new RulesModel(PayoutVariationEnum.JacksOrBetter_9_6, true, 1);
        this.gameModel = new GameModel(rules, 100, (int) System.DateTime.Now.Ticks);

        //hook up messages from the GameModel
        gameModel.updateBetUI = updatePlayerBetUI;
        gameModel.updateCardSlotUI = updateCardSlotUI;
        gameModel.updateCreditsUI = updatePlayerCreditsUI;
        gameModel.updatePlayButtonUI = updatePlayButtonUI;
        gameModel.updatePlayerHandUI = updatePlayerHandCardsUI;

        //Clear the dummy data
        updateAllUI();
    }

    #region Loading methods

    private void loadSprites() {
        spriteForCardIdLookup.Clear();
        foreach (var card in Constants.fullDeckPlusJokers) {
            var sp = Resources.Load<Sprite>(card.spritePath);
            spriteForCardIdLookup.Add(card.id, sp);
        }
        spriteForCardSlot = Resources.Load<Sprite>("Sprites/Cards/cardBack_green5");
    }

    #endregion

    #region Update UI methods

    private void updateAllUI() {
        patternListLabel.text = string.Join("\n", gameModel.rules.payoutVariation.Keys.Select(k => k.ToString()));

        updatePlayerBetUI();
        updatePlayerCreditsUI();
        updatePlayerHandCardsUI();
        updatePlayButtonUI();
    }

    private void updatePlayerBetUI() {
        //Show the player's bet
        var playerBet = gameModel.player.bet;
        betLabel.text = $"Bet: {playerBet}";
        payoutListLabel.text = string.Join("\n", gameModel.rules.payoutVariation.Values.Select(v => v * playerBet).Select(v => v.ToString()));
        updateChangeBetButtonsUI();
    }

    private void updateChangeBetButtonsUI() {
        decreaseBetButton.enabled = decreaseBetButton.interactable = gameModel.canDecreaseBet();
        increaseBetButton.enabled = increaseBetButton.interactable = gameModel.canIncreaseBet();
    }

    private void updatePlayerCreditsUI() {
        //Show the player's credits
        creditsLabel.text = $"{gameModel.player.credits} Credits";
    }
    private void updatePlayerHandCardsUI() {
        //Show the cards in the player's hand
        for (int index = 0; index < gameModel.rules.cardsInHand; index++) {
            updateCardSlotUI(index);
        }
    }
    private void updateCardSlotUI(int index) {
        var button = cardSlotButtons[index];
        if (!gameModel.isValidHandCardIndex(index)) {
            button.image.sprite = spriteForCardSlot;
            button.enabled = button.interactable = false;
            cardSlotLabels[index].text = string.Empty;
            return;
        }
        var card = gameModel.player.hand[index];
        button.image.sprite = spriteForCardIdLookup[card.card.id];
        button.enabled = button.interactable = (gameModel.getGameState() == GameModel.GameState.Game);
        cardSlotLabels[index].text = card.hold ? "Hold" : string.Empty;
    }

    private void updatePlayButtonUI() {
        switch (gameModel.getGameState()) {
            case GameModel.GameState.PreGame:
                playButtonLabel.text = "Deal";
                break;

            case GameModel.GameState.Game:
                var drawsLeft = gameModel.drawsLeft();
                if (drawsLeft == 1) {
                    playButtonLabel.text = "Draw";
                } else {
                    playButtonLabel.text = $"Draw ({drawsLeft} left)";
                }
                break;

            case GameModel.GameState.PostGame:
                playButtonLabel.text = "New Game";
                break;
        }
        updateChangeBetButtonsUI();
    }

    #endregion

    #region Button click methods

    public void increaseBetButtonClicked() {
        gameModel.increaseBet();
    }

    public void decreaseBetButtonClicked() {
        gameModel.decreaseBet();
    }

    public void playButtonClicked() {
        switch (gameModel.getGameState()) {
            case GameModel.GameState.PreGame:
                //Go into "Game" state
                gameModel.placeBet();
                gameModel.resetTheDeck();
                gameModel.shuffle();
                gameModel.deal();
                break;
            case GameModel.GameState.Game:
                gameModel.draw();
                if(gameModel.drawsLeft() == 0) {
                    //Go into "PostGame" state
                    gameModel.determineWinnings();
                }
                break;
            case GameModel.GameState.PostGame:
                //Go into "PreGame" state
                gameModel.discardHand();
                break;
        }
    }

    public void cardSlot1ButtonClicked() {
        gameModel.toggleCardSlotHold(0);
    }

    public void cardSlot2ButtonClicked() {
        gameModel.toggleCardSlotHold(1);
    }

    public void cardSlot3ButtonClicked() {
        gameModel.toggleCardSlotHold(2);
    }

    public void cardSlot4ButtonClicked() {
        gameModel.toggleCardSlotHold(3);
    }

    public void cardSlot5ButtonClicked() {
        gameModel.toggleCardSlotHold(4);
    }

    #endregion

}