﻿using System.Collections.Generic;
using System.Linq;
//#nullable enable

/// <summary>
/// Class to count the occurrences of items in a given collection
/// </summary>
public class Frequency<T>
{
    private Dictionary<T, ulong> dict = new Dictionary<T, ulong>();

    public Frequency() { }
    public Frequency(IEnumerable<T> instances) : this()
    {
        addAll(instances);
    }

    public ulong this[T instance]
    {
        get
        {
            if (dict.ContainsKey(instance))
            {
                return dict[instance];
            }
            return 0;
        }
    }

    public void addAll(IEnumerable<T> instances)
    {
        _ = instances.All(i => { add(i); return true; });
    }

    public void add(T instance, ulong increment = 1)
    {
        if (!dict.ContainsKey(instance))
        {
            dict.Add(instance, 0);
        }
        var currentValue = dict[instance];
        dict[instance] = currentValue + increment;
    }
    public void remove(T instance, ulong decrement = 1)
    {
        if (!dict.ContainsKey(instance))
        {
            return;
        }
        var currentValue = dict[instance];
        if (currentValue < decrement)
        {
            dict[instance] = 0;
        }
        dict[instance] = currentValue - decrement;
    }
    public void clear()
    {
        dict.Clear();
    }

    /// <summary>
    /// Return all the items with the highest number of occurrences
    /// </summary>
    public IEnumerable<T> highest()
    {
        var matchCount = highestCount();
        return dict.Where(pair => pair.Value == matchCount).Select(pair => pair.Key);
    }

    /// <summary>
    /// Return the highest number of occurrences
    /// </summary>
    public ulong highestCount()
    {
        if (dict.Values.Count == 0) return 0;
        return dict.Values.Max();
    }

    public IEnumerable<ulong> allCounts() {
        return dict.Values;
    }
}