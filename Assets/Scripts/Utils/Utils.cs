﻿using System;

//
//Other useful classes and code conveniences
//

//Mark as possible null
public class _nullableAttribute : Attribute { }

//Mark as never possible null
public class _nonnullAttribute : Attribute { }

//Deliberate crash
public class NotPossibleException : Exception { }
