﻿using System;
//#nullable enable

public partial struct PlayingCard {
    public readonly byte id;
    public readonly string spritePath;
    public readonly SuitEnum suit;
    public readonly CardValueEnum value;

    public PlayingCard(byte id, string spritePath, SuitEnum suit, CardValueEnum value) {
        this.id = id;
        this.spritePath = spritePath;
        this.suit = suit;
        this.value = value;
    }

}

public partial struct PlayingCard : IEquatable<PlayingCard> {
    public bool Equals(PlayingCard other) {
        return this.id == other.id;
    }
}
public partial struct PlayingCard : IComparable<PlayingCard> {
    public int CompareTo(PlayingCard other) {
        if (this.value == other.value) {
            return this.suit.CompareTo(other.suit);
        }
        return this.value.CompareTo(other.value);
    }
}
