﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
//#nullable enable

public class GameModel
{
    public Action updateBetUI;
    public Action updateCreditsUI;
    public Action<int> updateCardSlotUI;
    public Action updatePlayerHandUI;
    public Action updatePlayButtonUI;

    private System.Random random;
    public RulesModel rules;
    public PlayerModel player;
    private Deck deck;
    private Deck discard = new Deck();
    public GameResult? gameResult = null;

    public GameModel(RulesModel rules, long startingCredits, int seed)
    {
        this.rules = rules;
        this.player = new PlayerModel(startingCredits);
        this.random = new System.Random(seed);
    }

    public void resetTheDeck() {
        var deckToUse = rules.includeJokers ? Constants.fullDeckPlusJokers : Constants.fullDeck;
        this.deck = new Deck(deckToUse);
    }

    public bool canDecreaseBet() {
        return getGameState() == GameState.PreGame && player.canDecreaseBet();
    }
    public void decreaseBet() {
        player.decreaseBet();
        updateBetUI?.Invoke();
    }
    public bool canIncreaseBet() {
        return getGameState() == GameState.PreGame && player.canIncreaseBet();
    }
    public void increaseBet() {
        player.increaseBet();
        updateBetUI?.Invoke();
    }

    public void placeBet() {
        player.placeBet();
        updateCreditsUI?.Invoke();
    }

    public void shuffle() {
        this.deck.shuffle(this.random, 100);
    }

    public void deal() {
        player.numberOfDrawsTaken = 0;
        for (var i=0; i<rules.cardsInHand; i++) {
            var cardSlot = new PlayingCardSlot();
            cardSlot.card = deck.draw();
            player.hand.Add(cardSlot);
            updateCardSlotUI?.Invoke(i);
        }
        updatePlayButtonUI?.Invoke();
    }

    public void toggleCardSlotHold(int index) {
        if (getGameState() != GameState.Game) {
            return;
        }
        if (!isValidHandCardIndex(index)) {
            return;
        }
        player.hand[index].hold = !player.hand[index].hold;
        updateCardSlotUI?.Invoke(index);
    }

    public bool isValidHandCardIndex(int index) {
        return 0 <= index && index < player.hand.Count;
    }

    public void draw() {
        for (int index = 0; index < player.hand.Count; index++) {
            PlayingCardSlot slot = player.hand[index];
            if (!slot.hold) {
                //if (slot.card != null) {
                discard.Add(slot.card);
                //}
                var newCard = deck.draw();
                slot.card = newCard;
            }
            slot.hold = false;
            updateCardSlotUI?.Invoke(index);
        }
        player.numberOfDrawsTaken ++;
        updatePlayButtonUI?.Invoke();
    }

    public int drawsLeft() {
        return rules.numberOfDraws - player.numberOfDrawsTaken;
    }

    public void determineWinnings() {
        var gameResult = getGameResult();
        if(gameResult != null) {
            //Player won a payout!
            var winnings = player.bet * gameResult.Value.value;
            player.credits += winnings;
            Console.WriteLine($"Player won {winnings} credits with {gameResult.Value.pattern}!");
            updateCreditsUI?.Invoke();
        } else {
            Console.WriteLine($"No payout.");
        }

    }

    public void discardHand() {
        player.hand.Clear();
        updatePlayerHandUI?.Invoke();
        updatePlayButtonUI?.Invoke();
    }

    public enum GameState {
        PreGame,
        Game,
        PostGame,
        Error,
    }

    public GameState getGameState() {
        var numberCardsInHand = player.hand.Count();
        if (numberCardsInHand == 0) {
            return GameState.PreGame;
        }
        if (numberCardsInHand <= rules.cardsInHand) {
            if (player.numberOfDrawsTaken < rules.numberOfDraws) {
                return GameState.Game;
            }
            return GameState.PostGame;
        }
        return GameState.Error;
    }

    public struct GameResult {
        public PatternEnum pattern;
        public int value;
        public PlayingCard[] cards;

        public GameResult(PatternEnum pattern, int value, PlayingCard[] cards) {
            this.pattern = pattern;
            this.value = value;
            this.cards = cards;
        }
    }

    private GameResult? getGameResult() {
        var patternsToCheck = rules.payoutVariation.OrderBy(pair => pair.Value).Reverse();
        foreach(var pair in patternsToCheck) {
            var pattern = pair.Key;
            var method = rules.patternMatchingMethods.getMethodForPattern(pattern);
            var winningCards = method.Invoke(player.hand.Select(s => s.card).ToArray());
            if(winningCards != null) {
                return new GameResult(pattern, pair.Value, winningCards);
            }
        }
        return null;
    }
}