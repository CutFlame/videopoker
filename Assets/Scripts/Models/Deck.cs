﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
//#nullable enable

public class Deck : List<PlayingCard>
{
    public Deck() : base() { }
    public Deck(IEnumerable<PlayingCard> collection) : base(collection) { }

    public void shuffle(System.Random random, uint times = 20) {
        for(int i=0; i<times; i++) {
            selectionShuffle(random);
        }
    }

    public PlayingCard draw() {
        var card = this[0];
        this.RemoveAt(0);
        return card;
    }

    private void selectionShuffle(System.Random random) {
        for(int i=0; i<this.Count; i++) {
            var randomIndex = random.Next(this.Count);
            swapItemsAtIndexes(i, randomIndex);
        }
    }

    private void swapItemsAtIndexes(int index1, int index2) {
        var temp = this[index1];
        this[index1] = this[index2];
        this[index2] = temp;
    }

    //NOTE: this doesn't work
    private void riffleShuffle(System.Random random) {
        var firstHalf = this.Take(this.Count / 2).ToList();
        var secondHalf = this.Except(firstHalf).ToList();
        var result = new List<PlayingCard>();
        while (firstHalf.Count > 0 && secondHalf.Count > 0) {
            bool chooseFirst = random.Next(0, 2) == 0;
            var numberToTake = random.Next(0, 3);
            if (chooseFirst) {
                var toAdd = firstHalf.Take(numberToTake);
                result.AddRange(toAdd);
                firstHalf.RemoveRange(0, numberToTake);
            } else {
                var toAdd = secondHalf.Take(numberToTake);
                result.AddRange(toAdd);
                secondHalf.RemoveRange(0, numberToTake);
            }
        }
        var oldCount = this.Count;
        this.Clear();
        this.AddRange(result.Concat(firstHalf).Concat(secondHalf));
        Debug.Assert(this.Count == oldCount, "List changed size after shuffle");
    }
}
