﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//#nullable enable

public class PlayerModel
{
    public long credits;
    public long bet = Constants.minBet;
    public List<PlayingCardSlot> hand = new List<PlayingCardSlot>();
    public int numberOfDrawsTaken = 0;

    public PlayerModel(long credits)
    {
        this.credits = credits;
    }

    public bool canIncreaseBet() {
        return bet < Constants.maxBet;
    }
    public void increaseBet() {
        bet = Math.Min(bet + 1, Constants.maxBet);
    }
    public bool canDecreaseBet() {
        return bet > Constants.minBet;
    }
    public void decreaseBet() {
        bet = Math.Max(bet - 1, Constants.minBet);
    }

    public void placeBet() {
        credits -= bet;
    }
}