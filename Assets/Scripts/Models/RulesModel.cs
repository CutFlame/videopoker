﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//#nullable enable

public class RulesModel
{
    public bool includeJokers = true;
    public int numberOfDraws = 1;
    public byte cardsInHand = 5;
    public PayoutVariationEnum payoutVariationEnum;
    public PatternMatchingMethods patternMatchingMethods = new PatternMatchingMethods();
    public PayoutVariation payoutVariation {
        get { return Constants.payoutVariations[payoutVariationEnum]; }
    }

    public RulesModel(PayoutVariationEnum payoutVariationEnum, bool includeJokers, int numberOfDraws)
    {
        this.payoutVariationEnum = payoutVariationEnum;
        this.includeJokers = includeJokers;
        this.numberOfDraws = numberOfDraws;
    }
}
