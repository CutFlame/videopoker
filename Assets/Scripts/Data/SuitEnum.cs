﻿using System;
//#nullable enable

public enum SuitEnum : byte {
    Clubs,
    Hearts,
    Spades,
    Diamonds,
    Joker,
}

public static class SuitEnumExtensions {
    public static bool isSameAs(this SuitEnum lhs, SuitEnum rhs) {
        if (lhs == SuitEnum.Joker || rhs == SuitEnum.Joker) {
            return true;
        }
        return lhs == rhs;
    }
}
