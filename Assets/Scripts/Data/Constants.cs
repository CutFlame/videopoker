﻿using System;
using System.Collections.Generic;
using System.Linq;
//#nullable enable

public static class Constants {
    public const long minBet = 1;
    public const long maxBet = 5;

    public static readonly CardValueEnum[] royalCards = new CardValueEnum[] {
        CardValueEnum.Ace,
        CardValueEnum.Ten,
        CardValueEnum.Jack,
        CardValueEnum.Queen,
        CardValueEnum.King,
        CardValueEnum.Joker,
    };

    public static readonly PlayingCard[] fullDeck = new PlayingCard[] {
        new PlayingCard(10, "Sprites/Cards/cardClubsA", SuitEnum.Clubs, CardValueEnum.Ace),
        new PlayingCard(11, "Sprites/Cards/cardClubs2", SuitEnum.Clubs, CardValueEnum.Two),
        new PlayingCard(12, "Sprites/Cards/cardClubs3", SuitEnum.Clubs, CardValueEnum.Three),
        new PlayingCard(13, "Sprites/Cards/cardClubs4", SuitEnum.Clubs, CardValueEnum.Four),
        new PlayingCard(14, "Sprites/Cards/cardClubs5", SuitEnum.Clubs, CardValueEnum.Five),
        new PlayingCard(15, "Sprites/Cards/cardClubs6", SuitEnum.Clubs, CardValueEnum.Six),
        new PlayingCard(16, "Sprites/Cards/cardClubs7", SuitEnum.Clubs, CardValueEnum.Seven),
        new PlayingCard(17, "Sprites/Cards/cardClubs8", SuitEnum.Clubs, CardValueEnum.Eight),
        new PlayingCard(18, "Sprites/Cards/cardClubs9", SuitEnum.Clubs, CardValueEnum.Nine),
        new PlayingCard(19, "Sprites/Cards/cardClubs10", SuitEnum.Clubs, CardValueEnum.Ten),
        new PlayingCard(20, "Sprites/Cards/cardClubsJ", SuitEnum.Clubs, CardValueEnum.Jack),
        new PlayingCard(21, "Sprites/Cards/cardClubsQ", SuitEnum.Clubs, CardValueEnum.Queen),
        new PlayingCard(22, "Sprites/Cards/cardClubsK", SuitEnum.Clubs, CardValueEnum.King),

        new PlayingCard(30, "Sprites/Cards/cardHeartsA", SuitEnum.Hearts, CardValueEnum.Ace),
        new PlayingCard(31, "Sprites/Cards/cardHearts2", SuitEnum.Hearts, CardValueEnum.Two),
        new PlayingCard(32, "Sprites/Cards/cardHearts3", SuitEnum.Hearts, CardValueEnum.Three),
        new PlayingCard(33, "Sprites/Cards/cardHearts4", SuitEnum.Hearts, CardValueEnum.Four),
        new PlayingCard(34, "Sprites/Cards/cardHearts5", SuitEnum.Hearts, CardValueEnum.Five),
        new PlayingCard(35, "Sprites/Cards/cardHearts6", SuitEnum.Hearts, CardValueEnum.Six),
        new PlayingCard(36, "Sprites/Cards/cardHearts7", SuitEnum.Hearts, CardValueEnum.Seven),
        new PlayingCard(37, "Sprites/Cards/cardHearts8", SuitEnum.Hearts, CardValueEnum.Eight),
        new PlayingCard(38, "Sprites/Cards/cardHearts9", SuitEnum.Hearts, CardValueEnum.Nine),
        new PlayingCard(39, "Sprites/Cards/cardHearts10", SuitEnum.Hearts, CardValueEnum.Ten),
        new PlayingCard(40, "Sprites/Cards/cardHeartsJ", SuitEnum.Hearts, CardValueEnum.Jack),
        new PlayingCard(41, "Sprites/Cards/cardHeartsQ", SuitEnum.Hearts, CardValueEnum.Queen),
        new PlayingCard(42, "Sprites/Cards/cardHeartsK", SuitEnum.Hearts, CardValueEnum.King),

        new PlayingCard(50, "Sprites/Cards/cardSpadesA", SuitEnum.Spades, CardValueEnum.Ace),
        new PlayingCard(51, "Sprites/Cards/cardSpades2", SuitEnum.Spades, CardValueEnum.Two),
        new PlayingCard(52, "Sprites/Cards/cardSpades3", SuitEnum.Spades, CardValueEnum.Three),
        new PlayingCard(53, "Sprites/Cards/cardSpades4", SuitEnum.Spades, CardValueEnum.Four),
        new PlayingCard(54, "Sprites/Cards/cardSpades5", SuitEnum.Spades, CardValueEnum.Five),
        new PlayingCard(55, "Sprites/Cards/cardSpades6", SuitEnum.Spades, CardValueEnum.Six),
        new PlayingCard(56, "Sprites/Cards/cardSpades7", SuitEnum.Spades, CardValueEnum.Seven),
        new PlayingCard(57, "Sprites/Cards/cardSpades8", SuitEnum.Spades, CardValueEnum.Eight),
        new PlayingCard(58, "Sprites/Cards/cardSpades9", SuitEnum.Spades, CardValueEnum.Nine),
        new PlayingCard(59, "Sprites/Cards/cardSpades10", SuitEnum.Spades, CardValueEnum.Ten),
        new PlayingCard(60, "Sprites/Cards/cardSpadesJ", SuitEnum.Spades, CardValueEnum.Jack),
        new PlayingCard(61, "Sprites/Cards/cardSpadesQ", SuitEnum.Spades, CardValueEnum.Queen),
        new PlayingCard(62, "Sprites/Cards/cardSpadesK", SuitEnum.Spades, CardValueEnum.King),

        new PlayingCard(70, "Sprites/Cards/cardDiamondsA", SuitEnum.Diamonds, CardValueEnum.Ace),
        new PlayingCard(71, "Sprites/Cards/cardDiamonds2", SuitEnum.Diamonds, CardValueEnum.Two),
        new PlayingCard(72, "Sprites/Cards/cardDiamonds3", SuitEnum.Diamonds, CardValueEnum.Three),
        new PlayingCard(73, "Sprites/Cards/cardDiamonds4", SuitEnum.Diamonds, CardValueEnum.Four),
        new PlayingCard(74, "Sprites/Cards/cardDiamonds5", SuitEnum.Diamonds, CardValueEnum.Five),
        new PlayingCard(75, "Sprites/Cards/cardDiamonds6", SuitEnum.Diamonds, CardValueEnum.Six),
        new PlayingCard(76, "Sprites/Cards/cardDiamonds7", SuitEnum.Diamonds, CardValueEnum.Seven),
        new PlayingCard(77, "Sprites/Cards/cardDiamonds8", SuitEnum.Diamonds, CardValueEnum.Eight),
        new PlayingCard(78, "Sprites/Cards/cardDiamonds9", SuitEnum.Diamonds, CardValueEnum.Nine),
        new PlayingCard(79, "Sprites/Cards/cardDiamonds10", SuitEnum.Diamonds, CardValueEnum.Ten),
        new PlayingCard(80, "Sprites/Cards/cardDiamondsJ", SuitEnum.Diamonds, CardValueEnum.Jack),
        new PlayingCard(81, "Sprites/Cards/cardDiamondsQ", SuitEnum.Diamonds, CardValueEnum.Queen),
        new PlayingCard(82, "Sprites/Cards/cardDiamondsK", SuitEnum.Diamonds, CardValueEnum.King),
    };

    public static readonly PlayingCard[] fullDeckPlusJokers = fullDeck.Concat(new PlayingCard[] {
        new PlayingCard(90, "Sprites/Cards/cardJoker", SuitEnum.Joker, CardValueEnum.Joker),
        new PlayingCard(91, "Sprites/Cards/cardJoker", SuitEnum.Joker, CardValueEnum.Joker),
    }).ToArray();

    public static readonly Dictionary<PayoutVariationEnum, PayoutVariation> payoutVariations = new Dictionary<PayoutVariationEnum, PayoutVariation> {
        { PayoutVariationEnum.DoubleBonus, new PayoutVariation {
            { PatternEnum.RoyalFlush, 250 },
            { PatternEnum.StraightFlush, 50 },
            { PatternEnum.FourAces, 160 },
            { PatternEnum.FourOfAKind, 50 },
            { PatternEnum.FullHouse, 10 },
            { PatternEnum.Flush, 7 },
            { PatternEnum.Straight, 5 },
            { PatternEnum.ThreeOfAKind, 3 },
            { PatternEnum.TwoPair, 1 },
            { PatternEnum.JacksOrBetter, 1 },
        } },
        { PayoutVariationEnum.JacksOrBetter_9_6, new PayoutVariation {
            { PatternEnum.RoyalFlush, 800 },
            { PatternEnum.StraightFlush, 50 },
            { PatternEnum.FourOfAKind, 25 },
            { PatternEnum.FullHouse, 9 },
            { PatternEnum.Flush, 6 },
            { PatternEnum.Straight, 4 },
            { PatternEnum.ThreeOfAKind, 3 },
            { PatternEnum.TwoPair, 2 },
            { PatternEnum.JacksOrBetter, 1 },
        } },
        { PayoutVariationEnum.JacksOrBetter_9_5, new PayoutVariation {
            { PatternEnum.RoyalFlush, 800 },
            { PatternEnum.StraightFlush, 50 },
            { PatternEnum.FourOfAKind, 25 },
            { PatternEnum.FullHouse, 9 },
            { PatternEnum.Flush, 5 },
            { PatternEnum.Straight, 4 },
            { PatternEnum.ThreeOfAKind, 3 },
            { PatternEnum.TwoPair, 2 },
            { PatternEnum.JacksOrBetter, 1 },
        } },
        { PayoutVariationEnum.JacksOrBetter_8_6, new PayoutVariation {
            { PatternEnum.RoyalFlush, 800 },
            { PatternEnum.StraightFlush, 50 },
            { PatternEnum.FourOfAKind, 25 },
            { PatternEnum.FullHouse, 8 },
            { PatternEnum.Flush, 6 },
            { PatternEnum.Straight, 4 },
            { PatternEnum.ThreeOfAKind, 3 },
            { PatternEnum.TwoPair, 2 },
            { PatternEnum.JacksOrBetter, 1 },
        } },
    };
}