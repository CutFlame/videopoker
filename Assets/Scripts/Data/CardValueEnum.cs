﻿using System;
//#nullable enable

public enum CardValueEnum : byte {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
    Joker,
}

public static class CardValueEnumExtensions {
    public static bool isSameAs(this CardValueEnum lhs, CardValueEnum rhs) {
        if (lhs == CardValueEnum.Joker || rhs == CardValueEnum.Joker) {
            return true;
        }
        return lhs == rhs;
    }
}
