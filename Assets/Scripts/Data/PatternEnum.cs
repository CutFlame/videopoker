﻿
public enum PatternEnum {
    FiveOfAKind,
    RoyalFlush,
    StraightFlush,
    FourAces,
    FourOfAKind,
    FullHouse,
    Flush,
    Straight,
    ThreeOfAKind,
    TwoPair,
    JacksOrBetter,
    OnePair,
    HighCard,
}