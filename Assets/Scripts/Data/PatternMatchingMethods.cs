﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
//#nullable enable

[_nullable]
public delegate PlayingCard[] PatternMatchingDelegate(params PlayingCard[] cards);

public class PatternMatchingMethods {

    [_nonnull]
    public PatternMatchingDelegate getMethodForPattern(PatternEnum pattern) {
        switch (pattern) {
            case PatternEnum.FiveOfAKind: return fiveOfAKind;
            case PatternEnum.Flush: return flush;
            case PatternEnum.FourAces: return fourAces;
            case PatternEnum.FourOfAKind: return fourOfAKind;
            case PatternEnum.FullHouse: return fullHouse;
            case PatternEnum.JacksOrBetter: return onePairOfJacksOrBetter;
            case PatternEnum.OnePair: return onePair;
            case PatternEnum.RoyalFlush: return royalFlush;
            case PatternEnum.Straight: return straight;
            case PatternEnum.StraightFlush: return straightFlush;
            case PatternEnum.ThreeOfAKind: return threeOfAKind;
            case PatternEnum.TwoPair: return twoPair;                    
            //case PatternEnum.HighCard: return high
        }
        throw new NotPossibleException();
    }

    [_nullable]
    public PlayingCard[] fiveOfAKind(params PlayingCard[] cards) {
        if (allSameValue(cards) == true) return cards;
        return null;
    }

    private bool? allSameValue(IEnumerable<PlayingCard> cards) {
        if (cards.Count() == 0) return null;
        var lastCardValue = cards.First().value;
        foreach (var cardValue in cards.Select(c => c.value)) {
            if (!cardValue.isSameAs(lastCardValue)) return false;
            lastCardValue = cardValue;
        }
        return true;
    }

    private bool? allSameSuit(IEnumerable<PlayingCard> cards) {
        if (cards.Count() == 0) return null;
        var lastCardSuit = cards.First().suit;
        foreach (var cardSuit in cards.Select(c => c.suit)) {
            if (!cardSuit.isSameAs(lastCardSuit)) return false;
            lastCardSuit = cardSuit;
        }
        return true;
    }

    [_nullable]
    public PlayingCard[] straightFlush(params PlayingCard[] cards) {
        var isFlush = flush(cards) != null;
        if (!isFlush) return null;

        var isStraight = straight(cards) != null;
        if (!isStraight) return null;

        return cards;
    }

    private bool allRoyals(IEnumerable<PlayingCard> cards) {
        return cards.Select(c => c.value).All(Constants.royalCards.Contains);
    }

    [_nullable]
    public PlayingCard[] royalFlush(params PlayingCard[] cards) {
        if (!allRoyals(cards)) return null;
        var isFlush = flush(cards) != null;
        if (!isFlush) return null;
        return cards;
    }

    /*
     * https://www.geeksforgeeks.org/check-if-array-elements-are-consecutive/
     * The idea is to check for the following two conditions. If the following two conditions are true, then return true. 
     * 1) max – min + 1 = n where max is the maximum element in the array, min is the minimum element in the array and n is the number of elements in the array. 
     * 2) All elements are distinct.
     */
    [_nullable]
    public PlayingCard[] straight(params PlayingCard[] cards) {
        var cardsValues = cards.Select(c => c.value);
        var cardsNotIncludingJokers = cardsValues.Where(v => v != CardValueEnum.Joker);
        var counts = new Frequency<CardValueEnum>(cardsNotIncludingJokers);
        if (counts.highestCount() > 1) {
            return null;
        }
        var jokerCount = cardsValues.Where(v => v == CardValueEnum.Joker).Count();
        var min = cardsNotIncludingJokers.Min();
        var max = cardsNotIncludingJokers.Max();
        var minMaxDiffPlusOne = max - min + 1;
        var totalCards = cards.Count();
        if (isBetweenIncluding(minMaxDiffPlusOne, totalCards - jokerCount, totalCards)) {
            return cards;
        }
        return null;
    }

    private bool isBetweenIncluding(int value, int min, int max) {
        return min <= value && value <= max;
    }

    [_nullable]
    public PlayingCard[] flush(params PlayingCard[] cards) {
        if (allSameSuit(cards) == true) return cards;
        return null;
    }

    [_nullable]
    public PlayingCard[] fullHouse(params PlayingCard[] cards) {
        var three = threeOfAKind(cards);
        if (three == null) return null;
        var pair = onePair(cards.Except(three).ToArray());
        if (pair == null) return null;
        return three.Concat(pair).ToArray();
    }

    [_nullable]
    public PlayingCard[] fourAces(params PlayingCard[] cards) {
        var counts = new Frequency<CardValueEnum>(cards.Select(c => c.value));
        if (counts[CardValueEnum.Ace] + counts[CardValueEnum.Joker] >= 4) {
            return cards.Where(c => c.value == CardValueEnum.Ace || c.value == CardValueEnum.Joker).ToArray();
        }
        return null;
    }

    [_nullable]
    public PlayingCard[] fourOfAKind(params PlayingCard[] cards) {
        var highValue = valueThatHasMatches(cards, 4);
        if (highValue != null) {
            return cards.Where(c => c.value == highValue || c.value == CardValueEnum.Joker).ToArray();
        }
        return null;
    }

    private CardValueEnum? valueThatHasMatches(IEnumerable<PlayingCard> cards, ulong count) {
        if (cards.Count() == 0) return null;
        var counts = new Frequency<CardValueEnum>(cards.Select(c => c.value));
        var highValue = counts.highest().First();
        var highCount = counts[highValue];
        if (highCount + counts[CardValueEnum.Joker] == count) {
            return highValue;
        }
        return null;
    }

    [_nullable]
    public PlayingCard[] threeOfAKind(params PlayingCard[] cards) {
        var highValue = valueThatHasMatches(cards, 3);
        if (highValue != null) {
            return cards.Where(c => c.value == highValue || c.value == CardValueEnum.Joker).ToArray();
        }
        return null;
    }

    [_nullable]
    public PlayingCard[] onePair(params PlayingCard[] cards) {
        var highValue = valueThatHasMatches(cards, 2);
        if (highValue != null) {
            return cards.Where(c => c.value == highValue || c.value == CardValueEnum.Joker).ToArray();
        }
        return null;
    }

    [_nullable]
    public PlayingCard[] onePairOfJacksOrBetter(params PlayingCard[] cards) {
        var highValue = valueThatHasMatches(cards.Where(c => c.value >= CardValueEnum.Jack), 2);
        if (highValue != null) {
            return cards.Where(c => c.value == highValue || c.value == CardValueEnum.Joker).ToArray();
        }
        return null;
    }

    [_nullable]
    public PlayingCard[] twoPair(params PlayingCard[] cards) {
        var counts = new Frequency<CardValueEnum>(cards.Select(c => c.value));
        var highest = counts.highest()
            .ToList(); //Optimization: aggregating here so that it doesn't have to enumerate every time it's used in this function
        if (highest.Count() == 2) {
            return cards.Where(c => highest.Contains(c.value)).ToArray();
        }
        if (highest.Count() == 1 && counts[CardValueEnum.Joker] > 0) {
            var results = cards.Where(c => highest.Contains(c.value));
            var remaining = cards.Except(results);
            return results.Concat(highCardForEachJoker(remaining)).ToArray();
        }
        if (counts[CardValueEnum.Joker] > 1) {
            return highCardForEachJoker(cards);
        }
        return null;
    }

    [_nonnull]
    private PlayingCard[] highCardForEachJoker(IEnumerable<PlayingCard> cards) {
        var results = new List<PlayingCard>();
        var remaining = cards.ToList();
        while (remaining.Any(c => c.value == CardValueEnum.Joker) && remaining.Any(c => c.value != CardValueEnum.Joker)) {
            var joker = remaining.First(c => c.value == CardValueEnum.Joker);
            results.Add(joker);
            var highestCard = getHighestValueCard(remaining.Where(c => c.value != CardValueEnum.Joker));
            results.Add(highestCard);
            var toRemove = new[] { highestCard, joker };
            remaining = remaining.Except(toRemove).ToList();
        }
        return results.ToArray();
    }

    [_nonnull]
    private PlayingCard getHighestValueCard(IEnumerable<PlayingCard> cards) {
        return cards.Max();
        //var highValueCard = cards.First();
        //foreach(var card in cards) {
        //    if (card.CompareTo(highValueCard) == 1) {
        //        highValueCard = card;
        //    }
        //}
        //return highValueCard;
    }

}

public static class PatternMatchingMethodsTests {
    private static PlayingCard card(byte id) {
        foreach (var c in Constants.fullDeckPlusJokers) {
            if (c.id == id) return c;
        }
        throw new NotImplementedException($"No card with id: {id}");
        //return null;
    }
    private static PatternMatchingMethods methods = new PatternMatchingMethods();

    public static void run() {
        DebugAssertTrue(PatternEnum.FiveOfAKind, card(10), card(30), card(50), card(70), card(90));
        DebugAssertFalse(PatternEnum.FiveOfAKind, card(10), card(30), card(50), card(70), card(11));

        DebugAssertTrue(PatternEnum.StraightFlush, card(11), card(12), card(13), card(14), card(15));
        DebugAssertTrue(PatternEnum.StraightFlush, card(10), card(11), card(12), card(13), card(14)); //TODO: fix straight with Ace
        DebugAssertTrue(PatternEnum.StraightFlush, card(22), card(21), card(20), card(19), card(18));
        DebugAssertFalse(PatternEnum.StraightFlush, card(22), card(21), card(40), card(19), card(18));
        DebugAssertTrue(PatternEnum.StraightFlush, card(90), card(21), card(20), card(19), card(18)); // joker on end
        DebugAssertTrue(PatternEnum.StraightFlush, card(22), card(21), card(20), card(90), card(18)); // joker in middle
        DebugAssertTrue(PatternEnum.StraightFlush, card(22), card(91), card(20), card(90), card(18)); // two jokers in middle
        DebugAssertTrue(PatternEnum.StraightFlush, card(22), card(91), card(90), card(19), card(18)); // two jokers in middle
        DebugAssertTrue(PatternEnum.StraightFlush, card(90), card(91), card(20), card(19), card(18)); // two jokers on end
        DebugAssertTrue(PatternEnum.StraightFlush, card(22), card(21), card(20), card(90), card(91)); // two jokers on other end
        DebugAssertTrue(PatternEnum.StraightFlush, card(22), card(90), card(20), card(19), card(91)); // one joker in middle and one on the end

        DebugAssertTrue(PatternEnum.RoyalFlush, card(22), card(21), card(20), card(19), card(10));
        DebugAssertTrue(PatternEnum.RoyalFlush, card(42), card(41), card(40), card(39), card(30));
        DebugAssertTrue(PatternEnum.RoyalFlush, card(62), card(61), card(60), card(59), card(50));
        DebugAssertTrue(PatternEnum.RoyalFlush, card(82), card(81), card(80), card(79), card(70));

        DebugAssertTrue(PatternEnum.Straight, card(22), card(21), card(40), card(19), card(18));
        DebugAssertTrue(PatternEnum.Straight, card(31), card(12), card(13), card(14), card(15));
        DebugAssertTrue(PatternEnum.Straight, card(50), card(51), card(12), card(13), card(14)); //TODO: fix straight with Ace
        DebugAssertTrue(PatternEnum.Straight, card(90), card(21), card(20), card(19), card(18)); // joker on end
        DebugAssertTrue(PatternEnum.Straight, card(22), card(21), card(20), card(90), card(18)); // joker in middle
        DebugAssertTrue(PatternEnum.Straight, card(22), card(91), card(20), card(90), card(18)); // two jokers in middle
        DebugAssertTrue(PatternEnum.Straight, card(22), card(91), card(90), card(19), card(18)); // two jokers in middle
        DebugAssertTrue(PatternEnum.Straight, card(90), card(91), card(20), card(19), card(18)); // two jokers on end
        DebugAssertTrue(PatternEnum.Straight, card(22), card(21), card(20), card(90), card(91)); // two jokers on other end
        DebugAssertTrue(PatternEnum.Straight, card(22), card(90), card(20), card(19), card(91)); // one joker in middle and one on the end

        DebugAssertTrue(PatternEnum.Flush, card(10), card(11), card(14), card(16), card(20));

        DebugAssertTrue(PatternEnum.FullHouse, card(10), card(30), card(50), card(11), card(31));
        DebugAssertTrue(PatternEnum.FullHouse, card(10), card(30), card(50), card(90), card(31)); //one joker //TODO: fix this one
        DebugAssertTrue(PatternEnum.FullHouse, card(10), card(30), card(90), card(11), card(31)); //one joker
        DebugAssertTrue(PatternEnum.FullHouse, card(10), card(30), card(90), card(90), card(31)); //two jokers //TODO: fix this one

        DebugAssertTrue(PatternEnum.FourAces, card(10), card(30), card(50), card(70), card(22));
        DebugAssertTrue(PatternEnum.FourAces, card(22), card(30), card(50), card(70), card(10));
        DebugAssertTrue(PatternEnum.FourAces, card(22), card(30), card(50), card(91), card(10)); //one joker
        DebugAssertTrue(PatternEnum.FourAces, card(22), card(30), card(50), card(91), card(90)); //two jokers

        DebugAssertTrue(PatternEnum.FourOfAKind, card(10), card(30), card(50), card(70), card(22));
        DebugAssertTrue(PatternEnum.FourOfAKind, card(22), card(30), card(50), card(70), card(10));
        DebugAssertTrue(PatternEnum.FourOfAKind, card(22), card(30), card(50), card(91), card(10)); //one joker
        DebugAssertTrue(PatternEnum.FourOfAKind, card(22), card(30), card(50), card(91), card(90)); //two jokers

        DebugAssertTrue(PatternEnum.TwoPair, card(10), card(30), card(11), card(42), card(22));
        DebugAssertTrue(PatternEnum.TwoPair, card(22), card(30), card(50), card(42), card(10)); //TODO: fix this one
        DebugAssertTrue(PatternEnum.TwoPair, card(22), card(30), card(50), card(91), card(10)); //one joker
        DebugAssertTrue(PatternEnum.TwoPair, card(22), card(30), card(50), card(91), card(90)); //two jokers
    }

    private static void DebugAssertTrue(PatternEnum pattern, params PlayingCard[] cards) {
        var method = methods.getMethodForPattern(pattern);
        var results = method.Invoke(cards);
        if (results == null) {
            var cardsStr = string.Join(" ", cards.Select(c => Path.GetFileName(c.spritePath)));
            Console.WriteLine($"{pattern} failed with cards {cardsStr}");
        }
    }
    private static void DebugAssertFalse(PatternEnum pattern, params PlayingCard[] cards) {
        var method = methods.getMethodForPattern(pattern);
        var results = method.Invoke(cards);
        if (results != null) {
            var cardsStr = string.Join(" ", cards.Select(c => Path.GetFileName(c.spritePath)));
            Console.WriteLine($"{pattern} incorrect with cards {cardsStr}");
        }
    }
}